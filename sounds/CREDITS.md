# Credits

This is a list of authors of the sounds, links to their sources, associated licenses, and their original use intent in this project. Following headers relate to the file name which was preserved for tracking purposes.

The audio files were converted to `.webm` to conserve storage space and bandwidth usage.

## next.garyq.135434.industrial-warehouse-lights

| | |
|---|---|
|Author|[GaryQ](https://freesound.org/people/GaryQ/)|
|Source|<https://freesound.org/s/135434/>|
|License|[Creative Commons - Attribution](https://creativecommons.org/licenses/by/3.0/)|
|Usage|Next up|

## round.halgrimm.169867.swoosh

| | |
|---|---|
|Author|[Halgrimm](https://freesound.org/people/Halgrimm/)|
|Source|<https://freesound.org/s/169867/>|
|License|[Creative Commons - Zero](https://creativecommons.org/publicdomain/zero/1.0/)|
|Usage|Round cycling|

## round.zerono1.568141.switch-heavy-echo

| | |
|---|---|
|Author|[Zerono1](https://freesound.org/people/Zerono1/)|
|Source|<https://freesound.org/s/568141/>|
|License|[Creative Commons - Zero](https://creativecommons.org/publicdomain/zero/1.0/)|
|Usage|Round cycling (alt)|

## turn.angelkunev.521726.ak01mp-industrial-feelings

| | |
|---|---|
|Author|[angelkunev](https://freesound.org/people/angelkunev/)|
|Source|<https://freesound.org/s/521726/>|
|License|[Creative Commons - Attribution](https://creativecommons.org/licenses/by/3.0/)|
|Usage|Turn start (alt)|

## turn.unfa.550909.braam

| | |
|---|---|
|Author|[unfa](https://freesound.org/people/unfa/)|
|Source|<https://freesound.org/s/550909/>|
|License|[Creative Commons - Zero](https://creativecommons.org/publicdomain/zero/1.0/)|
|Usage|Turn start|
