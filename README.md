# Koboldworks – Ready Up!

Displays simple Next Up and Your Turn messages to players when appropriate with little sound effects to go along with it.

The volume is controllable in module settings.

## Rationale

Reminder for people to ready up(!) for their own turn.

## Screencaps

Next:  
![Next Up](./img/screencaps/next.png)

Now:  
![Your Turn](./img/screencaps/now.png)

## Install

Manifest URL: <https://gitlab.com/koboldworks/agnostic/ready-up/-/releases/permalink/latest/downloads/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).

## Credits

This module was inspired by [Combat Ready](https://github.com/smilligan93/combatready) module.

Audio content is credited in detail in [audio credits](./sounds/CREDITS.md).
