import { CFG } from './config.mjs';

const active = 'active', hidden = 'hidden', ghosted = 'ghosted', nextUp = 'next', turnStart = 'now';

export class Overlay {
	#overlay = document.createElement('div');
	dismissable = false;
	timerId;

	#labelNext;
	#labelNow;

	constructor() {
		this.#overlay.id = `${CFG.id}-overlay`;
		// this._overlay.classList.add(ghosted);
		this.#labelNext = document.createElement('label');
		this.#labelNow = document.createElement('label');
		const _container = document.createElement('div');
		this.#labelNow.id = `${CFG.id}-now`;
		this.#labelNow.textContent = game.i18n.localize('Koboldworks.ReadyUp.YourTurn')
		this.#labelNext.id = `${CFG.id}-next`;
		this.#labelNext.textContent = game.i18n.localize('Koboldworks.ReadyUp.NextUp');
		_container.classList.add('container');
		_container.append(this.#labelNow, this.#labelNext);
		this.#overlay.append(_container);
		this.#overlay.classList.add(hidden);
		document.body.append(this.#overlay);

		// $(this._overlay).on('transitionend', this._transitionEnd);
	}

	resetTimer() {
		clearTimeout(this.timerId);
		this.timerId = undefined;
	}

	resize() {
		const sbc = getComputedStyle(document.getElementById('sidebar'));
		this.#overlay.style.width = `calc(100% - ${sbc.width} - ${sbc.right})`; // render over all but the side bar
	}

	displayTime = Date.now();
	boundDismiss = this.dismiss.bind(this); // for event listener nonsense

	removeClickListener() {
		document.getElementById('board').removeEventListener('click', this.boundDismiss);
	}

	/*
	_transitionEnd(ev) {
		console.log(this);
		const s = getComputedStyle(ev.target);
		const opacity = parseFloat(s.opacity) ?? 1;
	}
	*/

	dismiss(ev) {
		if (Date.now() - this.displayTime < 250) return; // Ignore clicks too soon after displaying.

		this.dismissable ? this.hide() : this.ghost();

		const o = this.#overlay;
		if (!o.classList.contains(ghosted)) {
			// Prevent event bubbling
			ev?.preventDefault();
			ev?.stopPropagation();
		}
	}

	hide() {
		this.resetTimer();
		this.removeClickListener();
		const o = this.#overlay;
		o.classList.remove(active);
		o.classList.add(hidden);
	}

	show({ now = false, dismissable = false, combatant = null, nextCombatant = null }) {
		this.displayTime = Date.now();

		const o = this.#overlay;
		if (now) {
			const msg = game.settings.get(CFG.id, CFG.SETTINGS.turnStartMessage);
			this.#labelNow.textContent = msg.replace('{name}', combatant?.name).replace('\\n', '\n');
			o.classList.add(turnStart);
			o.classList.remove(nextUp);
		}
		else {
			const msg = game.settings.get(CFG.id, CFG.SETTINGS.nextUpMessage);
			this.#labelNext.textContent = msg.replace('{name}', nextCombatant?.name).replace('\\n', '\n');
			o.classList.add(nextUp);
			o.classList.remove(turnStart);
		}

		const sticky = game.settings.get(CFG.id, CFG.SETTINGS.stickyKey);
		this.dismissable = sticky && dismissable || !sticky;
		// o.classList.remove(ghosted);
		this.resize();

		document.getElementById('board').addEventListener('click', this.boundDismiss);

		o.classList.remove(ghosted);
		o.classList.add(active);
		o.classList.remove(hidden);

		const linger = game.settings.get(CFG.id, CFG.SETTINGS.lingerKey);
		if (linger > 0.1) {
			this.resetTimer();
			this.timerId = setTimeout(this.boundDismiss, linger * 1_000);
		}
	}

	ghost() {
		this.resetTimer();
		this.removeClickListener();
		const o = this.#overlay;
		o.classList.add(ghosted);
	}

	/** Reset and remove current overlay */
	static delete() {
		const s = Overlay.instance;
		if (s) {
			s.resetTimer();
			s.#overlay?.remove();
			s.#overlay = null;
		}
		Overlay.instance = null;
	}

	/** @type {undefined|Overlay} */
	static instance;
}
