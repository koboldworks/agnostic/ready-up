/* Common keys. DO NOT MODIFY!*/

export const CFG = {
	id: 'koboldworks-ready-up',
	SETTINGS: {
		volumeKey: 'volume',
		lingerKey: 'linger',
		notifyKey: 'notify',
		stickyKey: 'sticky',
		spamWardKey: 'spamGuard',
		turnStartMessage: 'turnStart',
		nextUpMessage: 'nextUp',
	}
};
