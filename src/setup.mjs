import { CFG } from './config.mjs';
import { getPath } from './common.mjs';
import { Overlay } from './overlay.mjs';
import { updateAudioVolume, soundCfg, loadSound } from './audio.mjs';

// Initialize settings
Hooks.once('init', () => {
	game.settings.register(
		CFG.id,
		CFG.SETTINGS.volumeKey,
		{
			name: 'Koboldworks.ReadyUp.VolumeLabel',
			hint: 'Koboldworks.ReadyUp.VolumeHint',
			scope: 'client',
			config: true,
			type: Number,
			range: { min: 0.0, max: 100.0, step: 0.1 },
			default: 50.0,
			onChange: updateAudioVolume,
		}
	);

	for (const [key, settings] of Object.entries(soundCfg)) {
		game.settings.register(
			CFG.id,
			settings.key,
			{
				name: `Koboldworks.ReadyUp.${key.capitalize()}Sfx`,
				hint: `Koboldworks.ReadyUp.${key.capitalize()}SfxHint`,
				scope: 'world',
				config: true,
				type: String,
				default: getPath(settings.default),
				filePicker: true, // >=0.8.x file picker
				onChange: (v) => loadSound(key, v),
			}
		);

		// const sfx =
		loadSound(key, game.settings.get(CFG.id, settings.key));
		// console.log("READY UP | Registered sound:", key, sfx.file);
	}

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.notifyKey,
		{
			name: 'Koboldworks.ReadyUp.NotifyLabel',
			hint: 'Koboldworks.ReadyUp.NotifyHint',
			scope: 'client',
			config: true,
			type: Boolean,
			default: true,
			onChange: (value) => value ? Overlay.instance = new Overlay() : Overlay.delete()
		}
	);

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.lingerKey,
		{
			name: 'Koboldworks.ReadyUp.LingerLabel',
			hint: 'Koboldworks.ReadyUp.LingerHint',
			scope: 'client',
			config: true,
			type: Number,
			range: { min: 0.0, max: 25.0, step: 0.2 },
			default: 0.0,
		}
	);

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.stickyKey,
		{
			name: 'Koboldworks.ReadyUp.StickyLabel',
			hint: 'Koboldworks.ReadyUp.StickyHint',
			scope: 'client',
			config: true,
			type: Boolean,
			default: true,
		}
	);

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.spamWardKey,
		{
			name: 'Koboldworks.ReadyUp.SpamGuardLabel',
			hint: 'Koboldworks.ReadyUp.SpamGuardHint',
			scope: 'client',
			config: true,
			type: Boolean,
			default: true,
		}
	);

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.nextUpMessage,
		{
			name: 'Koboldworks.ReadyUp.NextUpLabel',
			hint: 'Koboldworks.ReadyUp.NextUpHint',
			scope: 'world',
			config: true,
			default: 'Next Up!',
			type: String,
		}
	);

	game.settings.register(
		CFG.id,
		CFG.SETTINGS.turnStartMessage,
		{
			name: 'Koboldworks.ReadyUp.TurnStartLabel',
			hint: 'Koboldworks.ReadyUp.TurnStartHint',
			scope: 'world',
			config: true,
			default: 'Your Turn!',
			type: String
		}
	);
});

Hooks.once('ready', () => {
	if (game.user.isGM) {
		// Migrate old variable usage
		const msg0 = game.settings.get(CFG.id, CFG.SETTINGS.turnStartMessage),
			msg0b = msg0.replace('${name}', '{name}'),
			msg0m = msg0 !== msg0b;
		if (msg0m) game.settings.set(CFG.id, CFG.SETTINGS.turnStartMessage, msg0b);
		const msg1 = game.settings.get(CFG.id, CFG.SETTINGS.nextUpMessage),
			msg1b = msg1.replace('${name}', '{name}'),
			msg1m = msg1 !== msg1b;
		if (msg1m) game.settings.set(CFG.id, CFG.SETTINGS.nextUpMessage, msg1b);

		if (msg1m || msg0m) {
			const msg = 'READY UP | Data Migrated';
			console.log(msg);
		}
	}
});
