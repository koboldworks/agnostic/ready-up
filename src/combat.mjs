import { CFG } from './config.mjs';
import { Overlay } from './overlay.mjs';
import { audio } from './audio.mjs';
import { getDocData } from './compat.mjs';

class CombatStatus {
	id;
	round;
	combatant;
	constructor(combat) {
		this.id = combat.id;
		this.round = combat.round;
		this.combatant = combat.combatant.id;
	}
}

let /** @type {CombatStatus} */
	lastCombat = null,
	/** @type {CombatStatus} */
	lastTurn = null,
	/** @type {CombatStatus} */
	lastTurnAnticipation = null;

function deleteCombatEvent() {
	Overlay.instance?.hide(); // Hide previous banner no matter what
}

async function updateCombatEvent(combat, _update, _options, _userId) {
	const combatData = getDocData(combat);
	if (!combatData.active) return;
	if (!combat.started) return;

	Overlay.instance?.hide(); // Hide previous banner

	const spamGuard = game.settings.get(CFG.id, CFG.SETTINGS.spamWardKey);
	const newCombatStatus = spamGuard ? new CombatStatus(combat) : null;

	// New Round
	if (combat.turn === 0) {
		if (!(lastCombat?.id === combat.id && lastCombat?.round === combat.round))
			audio.round?.play();
		lastCombat = newCombatStatus;
	}

	if (game.user.isGM) return; // GM doesn't need the rest

	const combatant = combat.combatant,
		currentPlayer = combatant.actor?.isOwner ?? false;

	// Current player's turn
	if (currentPlayer) {
		if (combatant.isDefeated) return;
		if (!(lastTurn?.id === combat.id &&
			lastTurn?.round === combat.round &&
			lastTurn?.combatant === combatant.id)) {
			audio.turn?.play();
			Overlay.instance?.show({ now: true, dismissable: true, combatant });
		}
		lastTurn = newCombatStatus;
		return;
	}

	if (combat.turns.length < 3) return; // don't bother with next up

	const nextTurn = (combat.turn + 1) % combat.turns.length;
	const nextCombatant = combat.turns[nextTurn];

	const nextPlayer = nextCombatant.actor?.isOwner ?? false;
	// if (currentPlayer && nextPlayer) return; // same player for current and next turn

	// Current player is next
	if (nextPlayer) {
		if (nextCombatant.isDefeated) return;
		if (!(lastTurnAnticipation?.id === combat.id &&
			lastTurnAnticipation?.round === combat.round &&
			lastTurnAnticipation?.combatant === combat.combatant.id)) {
			audio.next?.play();
			Overlay.instance?.show({ now: false, dismissable: false, nextCombatant });
		}
		lastTurnAnticipation = newCombatStatus;
	}
}

export async function assessActiveCombat({ sound = false } = {}) {
	// Unknown how well this works with multiple combats
	if (game.combats?.active)
		await updateCombatEvent(game.combats.active);
}

function updateCombatantEvent(_combatant, update, _options, _userId) {
	if (update.initiative) {
		Overlay.instance?.hide();
		Hooks.once('renderCombatTracker', assessActiveCombat); // assumption
	}
}

Hooks.on('updateCombat', updateCombatEvent);
Hooks.on('updateCombatant', updateCombatantEvent);
Hooks.on('deleteCombat', deleteCombatEvent);
