/**
 * @compatibility v9/v10 cross-compatibility
 */
export const getDocData = (doc)=>game.release.generation >= 10 ? doc : doc.data;
