import { CFG } from './config.mjs';

/** Default sound config */
export const soundCfg = {
	turn: { key: 'sfx-turn-file', default: 'turn.unfa.550909.braam.webm' },
	round: { key: 'sfx-round-file', default: 'round.halgrimm.169867.swoosh.webm' },
	next: { key: 'sfx-next-file', default: 'next.garyq.135434.industrial-warehouse-lights.webm' }
};

/**
 * Simplified layer over HTML Audio and Foundry's AudioHelper
 */
class SoundProxy {
	key;
	file;
	audio;

	constructor(key, file, volume = 0.5) {
		this.key = key;
		this.file = file;

		// this.audio = game.audio.create({ src: file, preload: true }); // doesn't exist?
		this.audio = new Audio(file);
		this.audio.preload = 'auto';
		this.audio.defaultMuted = true;
		this.audio.autoplay = false;
		this.audio.loop = false;
		this.audio.volume = Math.clamped(volume, 0.0, 1.0);

		this.audio.addEventListener('error', this.errorHandler.bind(this), { passive: true });
		this.audio.addEventListener('canplay', this.onLoadBound, { passive: true });
	}

	onLoadBound = this.onLoad.bind(this);
	/** Temporary onload handler  */
	onLoad() {
		this.play = this.playReal;
		this.audio.removeEventListener('canplay', this.onLoadBound);
		if (this.queued) this.play();
		delete this.onLoadBound;
		delete this.queued;
		delete this.onLoad;
	}

	errorHandler() {
		const err = this.audio.error;
		if (err.code === MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED ||
			err.code === MediaError.MEDIA_ERR_NETWORK && this.audio.networkState === 3) {
			console.warn(`READY UP | ${this.key} | Failed to load "${this.file}"`);
		}
	}

	set volume(value) {
		this.audio.volume = value;
	}

	queued = false;
	/** Temporary until onLoad happens */
	play = function () { this.queued = true; }; // placeholder until loaded

	async playReal(volume = null) {
		if (volume) this.volume = volume;
		// this.audio.play({ offset: 0, volume: volume ?? this.volume }); // AudioHelper

		if (!this.audio.paused)
			this.reset();

		return this.audio.play();
	}

	reset() {
		this.audio.currentTime = 0;
	}
}

export const audio = {
	next: undefined,
	round: undefined,
	turn: undefined,
};

export function updateAudioVolume(value) {
	const newVolume = Math.clamped(value / 100, 0.0, 1.0);
	console.log('READY UP | Volume | Set:', value, '%');
	for (const s of Object.values(audio))
		s.volume = newVolume;
}

export function loadSound(key, file) {
	audio[key] = file ? new SoundProxy(key, file, game.settings.get(CFG.id, CFG.SETTINGS.volumeKey) / 100) : null;
	return audio[key];
}

let nextSound = -1;

/**
 *
 * @param {Event} ev
 * @param {Element} el
 * @returns
 */
export async function audioTest(ev, el) {
	ev.preventDefault();
	ev.stopPropagation();

	const volumeVal = el.querySelector(`input[name="${CFG.id}.volume"]`).value;
	const volume = parseFloat(volumeVal) / 100;

	const cfg = Object.values(soundCfg);
	nextSound = (nextSound + 1) % cfg.length;

	const key = Object.keys(soundCfg)[nextSound];
	const file = el.querySelector(`input[name="${CFG.id}.${cfg[nextSound].key}"]`)?.value ?? audio[key].file;

	const tmp = file ? new SoundProxy('test', file, volume) : null;
	if (file) ui.notifications.info(game.i18n.format('Koboldworks.ReadyUp.TestNotify', { key, file: tmp.file, volume: Math.floor(volume * 1_000) / 10 }));
	else ui.notifications?.info(`No file specified for ${cfg[nextSound].key} to play it.`);
	return tmp?.play();
}

// Add sound test button to module config
/**
 * @param {JQuery} jq
 */
Hooks.on('renderSettingsConfig', (_sheet, jq) => {
	const html = jq[0];
	const p = html.querySelector(`input[name="${CFG.id}.volume"]`)?.parentElement;
	const button = document.createElement('button');
	button.type = 'button';
	button.style.cssText += 'margin-left:3px;width:4em;';
	button.textContent = game.i18n.localize('Koboldworks.ReadyUp.Test');
	button.addEventListener('click', ev => audioTest(ev, html));
	p.append(button);
});
