import { CFG } from './config.mjs';

export const getPath = (file) => `modules/${CFG.id}/sounds/${file}`;
