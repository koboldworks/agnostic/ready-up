import './setup.mjs';
import { CFG } from './config.mjs';
import { Overlay } from './overlay.mjs';
import { assessActiveCombat } from './combat.mjs';

Hooks.once('ready', async () => {
	if (game.settings.get(CFG.id, CFG.SETTINGS.notifyKey))
		Overlay.instance = new Overlay();

	// Prepare for on-load situation to make sure nothing is missed
	assessActiveCombat();

	console.log('READY UP | Ready!');
});
